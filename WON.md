# the sound of WON hand fapping 

**won** 
>  past and past participle of win.

His was the classic rags to riches story, except the rag was covered in semen, folded into a [pocket square](https://www.ties.com/how-to-fold-a-pocket-square). 

The first work was an empty apartment overlooking city center. For years the six hunderd square foot apartment, lay barren until one day a folding chair appeared. At the right angle, on the top of a hill that marked the exact center of center, a clear view of the lonely chair presented itself. Months passed; the chair insignicant to even the most observant stream of foot traffic below.  

![Empty Room](/images/room_one.png)

One day a masked man appeared. Later they would say he began fully clothed but like all folklore; bullshit. Oh the horror, when, after a few weeks of having supposedly sat fully clothed/masked, the man had misplaced pants and underwear. He now had on nothing but mask, shirt, socks and what many believed to be the shit-eating grin that accompanied an inappropriate erection.

Traditional media refused to dignify the masked erection with a story, anonymity being the crime that it was. Naturally the anonymity virus grew to an international scale when authorities realized that the spectacle was driving renewed tourist interest to the city center. Local businesses would sing his praises for generations after a significant upturn in sales coincided with his arrival.

Anonymity as crime was turning into anonymity as cool. The blue and red mask was an international best-seller. Children asked their parents if they could wear it to school. The masked erection man was an outlet for an entire globe in which "Open Idenity" was mandatory.Though we couldn't do it, he had the courage to do it for us. Coincidently, the exact moment that anonymity became a global cool, the machines had proved effective at masked face detection. 

Copycat erections popped up all over the globe. Better erections, nicer masks, more professional sock choices, but there was no competing with the original. Even with better parts they all missed the shit-eating grin you could see, even if you couldn't actually see it. 

And at just the right moment he disappeared, never to return. He left a folding chair, a white t-shirt, and a pair of socks. For weeks suckers bought fraudelent "authentic" folding chairs, white t-shirts and socks that the locals had just picked up from the big box.

This was only won. He had not figured out how to monetize yet. But it would come. It always came. 

